package org.opencv.samples.colorblobdetect;

import java.util.ArrayList;
import java.util.List;

import org.opencv.android.JavaCameraView;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.util.Log;

public class ColorBlobDetector extends JavaCameraView {
	// Constructor
	public ColorBlobDetector(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	// Constants
	private int LED_COUNT = 144;
	private int TEST_SPACING = 3;
	
	// More constants
	private Scalar empty = new Scalar(0, 0, 0);
	private Size blurSize = new Size(5, 5);
	
	// Network communication variables
	private int[] returnArray = new int[6];
	int[] indices = new int[2];
	int[] endIndices = new int[2];
	private int center = LED_COUNT / 2;

	// Image variables
	private Mat binaryFrameRed;
	private Mat binaryFrameGreen;
	private Mat binaryFrameNotDark;
	private Mat binaryFrameWhite;

	// Vector variables
	List<MatOfPoint> contours0_red = new ArrayList<MatOfPoint>();
	List<MatOfPoint> contours0_green = new ArrayList<MatOfPoint>();

	// Camera parameters
	Camera.Parameters params;

	// LED counting variables
	private boolean switched = true;
	public int[] ledCounts = new int[2];

	// Returns the start and end indices to display on LED strip
	public int[] getIndices(double a, double b) {
		// Compute indices
		/*indices[0] = (LED_COUNT / 2)
				- (((ledCounts[0] * TEST_SPACING) + (ledCounts[1] * TEST_SPACING)) / 2)
				+ (((ledCounts[0] * TEST_SPACING) - (ledCounts[1] * TEST_SPACING)) / 2);
		indices[1] = (LED_COUNT / 2)
				+ (((ledCounts[0] * TEST_SPACING) + (ledCounts[1] * TEST_SPACING)) / 2)
				+ (((ledCounts[0] * TEST_SPACING) - (ledCounts[1] * TEST_SPACING)) / 2);*/
		
		indices[0] = (center - ledCounts[1] * TEST_SPACING);
		indices[1] = (center + ledCounts[0] * TEST_SPACING - 1);

		// Recompute center
		center = (indices[0] + indices[1] + 1) / 2;
		
		// Reset center if necessary
		if(center < 0) {
			center = 0;
		} else if(center > LED_COUNT) {
			center = LED_COUNT;
		}

		// Compute new indices scaled and offset by values from settings
		//endIndices[0] = (int) (a * (Math.abs(indices[1] - indices[0])) + b + indices[0] + indices[1]) / 2;
		//endIndices[1] = (int) (indices[0] + indices[1] - (a * (Math.abs(indices[1] - indices[0])) + b)) / 2;
		
		endIndices[0] = (int) (center - ((center - indices[0]) * a) + b);
		endIndices[1] = (int) (center + ((indices[1] - center) * a) + b);
		
		// Write indices to array
		for(int i = 0; i < 3; i++) {
			returnArray[i] = (int) ((endIndices[0] % Math.pow(10, (3 - i))) / Math.pow(10, (2 - i)));
			returnArray[i + 3] = (int) ((endIndices[1] % Math.pow(10, (3 - i))) / Math.pow(10, (2 - i)));
		}

		return returnArray;
	}

	// Returns the current LED counts
	public int[] getLedCounts() {
		return ledCounts;
	}

	// Processes an image and counts how many LEDs are visible in it
	public void process(Mat rgbaImage) {
		// Reset values initially
		ledCounts[0] = 0;
		ledCounts[1] = 0;

		// Constants
		int THRESH_BRIGHTNESS_MIN = 100; // 50
		int THRESH_SATURATION_MAX = 50;
		int THRESH_VALUE_MIN = 100;
		int THRESH_RED_LOW = 20;
		int THRESH_RED_HIGH = 160;
		int THRESH_GREEN_LOW = 35;
		int THRESH_GREEN_HIGH = 75;

		// Image variables
		List<Mat> channel = new ArrayList<Mat>();
		binaryFrameRed = new Mat();
		binaryFrameGreen = new Mat();
		binaryFrameNotDark = new Mat();
		binaryFrameWhite = new Mat();
		Mat tempFrame1 = new Mat();
		Mat tempFrame2 = new Mat();
		Mat hierarchy = new Mat();
		Mat frm_hsv = new Mat();

		// Vector variables
		contours0_red.clear();
		contours0_green.clear();
		
		// Calibrate switch
		if(ColorBlobDetectionActivity.result == 49 && switched == true) {
			switched = false;
		}

		// Convert frame from BGR into HSV format
		Imgproc.cvtColor(rgbaImage, frm_hsv, Imgproc.COLOR_RGBA2BGR); // Saves the original as well
		Imgproc.cvtColor(frm_hsv, frm_hsv, Imgproc.COLOR_BGR2HSV);

		// Split the image into H, S, and V channels for thresholding
		Core.split(frm_hsv, channel);

		// Filter out only non-dark areas for analysis
		Imgproc.threshold(channel.get(2), binaryFrameNotDark,
				THRESH_BRIGHTNESS_MIN, 255, Imgproc.THRESH_BINARY); // Value must be high enough

		// Find the white-ish regions of the image to subtract from red and green frames
		Imgproc.threshold(channel.get(1), tempFrame1, THRESH_SATURATION_MAX,
				255, Imgproc.THRESH_BINARY_INV); // Saturation needs to be low
		Imgproc.threshold(channel.get(2), tempFrame2, THRESH_VALUE_MIN, 255,
				Imgproc.THRESH_BINARY); // Value needs to be high
		Core.bitwise_and(tempFrame1, tempFrame2, binaryFrameWhite); // Combine to get white-ish regions

		// Process red frame
		tempFrame1.setTo(empty);
		tempFrame2.setTo(empty);
		Imgproc.threshold(channel.get(0), tempFrame1, THRESH_RED_LOW, 255,
				Imgproc.THRESH_BINARY_INV); // Low bound
		Imgproc.threshold(channel.get(0), tempFrame2, THRESH_RED_HIGH, 255,
				Imgproc.THRESH_BINARY); // High bound
		Core.add(tempFrame1, tempFrame2, binaryFrameRed); // Combine to get red regions
		Core.bitwise_and(binaryFrameRed, binaryFrameNotDark, binaryFrameRed); // Get rid of regions that are too dark
		// binaryFrameRed -= binaryFrameWhite; // Get rid of white-ish regions
		Imgproc.GaussianBlur(binaryFrameRed, binaryFrameRed, blurSize, 0, 0); // Blur image to remove noise

		// Process green frame
		tempFrame1.setTo(empty);
		tempFrame2.setTo(empty);
		Imgproc.threshold(channel.get(0), tempFrame1, THRESH_GREEN_LOW, 255,
				Imgproc.THRESH_BINARY); // Low bound
		Imgproc.threshold(channel.get(0), tempFrame2, THRESH_GREEN_HIGH, 255,
				Imgproc.THRESH_BINARY); // High bound
		Core.subtract(tempFrame1, tempFrame2, binaryFrameGreen); // Combine to get green regions
		Core.bitwise_and(binaryFrameGreen, binaryFrameNotDark, binaryFrameGreen); // Get rid of regions that are too dark
		// binaryFrameGreen -= binaryFrameWhite; // Get rid of white-ish regions
		Imgproc.GaussianBlur(binaryFrameGreen, binaryFrameGreen, blurSize, 0, 0); // Blur image to remove noise

		// Copy binary red and green frames since contour finding will destroy
		// them
		tempFrame1.setTo(empty);
		tempFrame2.setTo(empty);
		tempFrame1 = binaryFrameRed.clone(); // Copy red
		tempFrame2 = binaryFrameGreen.clone(); // Copy green

		// Count the number of objects in the red and green binary frames
		Imgproc.findContours(binaryFrameRed, contours0_red, hierarchy,
				Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
		Imgproc.findContours(binaryFrameGreen, contours0_green, hierarchy,
				Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
		if (switched) {
			ledCounts[0] = contours0_green.size();
			ledCounts[1] = contours0_red.size();
		} else {
			ledCounts[0] = contours0_red.size();
			ledCounts[1] = contours0_green.size();
		}

		// Get the original binary frames back
		binaryFrameRed = tempFrame1.clone();
		binaryFrameGreen = tempFrame2.clone();

		Log.d("information", "Size of frame: " + rgbaImage.cols() + "x" + rgbaImage.rows());
		
		// Print LED counts
		Log.d("information", "Red count: " + Integer.toString(ledCounts[0]));
		Log.d("information", "Green count: " + Integer.toString(ledCounts[1]));
		
		//switched = !switched; // Switching causes jitter

		// Clean up everything
        for(int i = 0; i < channel.size(); i ++) {
        	channel.get(i).release();
        }
		binaryFrameRed.release();
		binaryFrameGreen.release();
		binaryFrameNotDark.release();
		binaryFrameWhite.release();
		tempFrame1.release();
		tempFrame2.release();
		hierarchy.release();
		frm_hsv.release();
	}

	// Returns red contours
	public List<MatOfPoint> getRedContours() {
		return contours0_red;
	}

	// Returns green contours
	public List<MatOfPoint> getGreenContours() {
		return contours0_green;
	}

	// Returns current camera exposure
	public String getExposure() {
		return String.valueOf(mCamera.getParameters().getExposureCompensation());
	}
	
	// Prints all supported preview sizes for camera
	private void supportedSizes(){
	    //Camera.Parameters params = cam.getParameters();
	    List<Camera.Size> suppSizes = params.getSupportedPreviewSizes();
	    
	    for (int s = 0; s < suppSizes.size(); s ++){
	        Log.d("information", "Supported size: " + suppSizes.get(s).width + " x " + suppSizes.get(s).height);
	    }
	}

	// Configures the camera using specified input parameter
	@SuppressLint("NewApi")
	public void configureCamera(String change) {
		params = mCamera.getParameters();

		// Turns on auto-exposure
		if (change.equals("unlock auto-exposure")) {

			params.setAutoExposureLock(false);
			params.setExposureCompensation(params.getMinExposureCompensation()); // Set exposure to minimum as well

			Log.d("information", "Minimum exposure is " + params.getMinExposureCompensation());
			Log.d("information", "Maximum exposure is " + params.getMaxExposureCompensation());
			Log.d("information", "Current exposure is " + params.getExposureCompensation());
			
		// Turns off auto-exposure
		} else if (change.equals("lock auto-exposure")) {
			params.setExposureCompensation(params.getMinExposureCompensation());
			params.setAutoExposureLock(true);
			
		// Turns off some automatic features such as white balance, but not auto-exposure
		} else if (change.equals("lock camera")) {
			params.setAutoWhiteBalanceLock(true);
			params.setFocusMode(params.FOCUS_MODE_FIXED);
			
		// Resizes frame to 640x480
		} else if (change.equals("resize")) {
			supportedSizes();
			params.setPreviewSize(640, 480);
			Log.d("information", "Set camera size");
		}
		
		mCamera.setParameters(params);
	}
}
