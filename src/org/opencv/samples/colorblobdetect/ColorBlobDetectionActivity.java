package org.opencv.samples.colorblobdetect;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.List;

import org.abaq.wirelessveillometer.R;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.app.AlertDialog;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class ColorBlobDetectionActivity extends Activity implements OnTouchListener, CvCameraViewListener2 {
	
    // Mode of operation
    int modeOfOperation = 0;

    // Image variables
    private Mat newFrame;
    private Mat lastProcessedFrame;
    private Mat currentFrame;
    
    // Camera variables
    Camera.Parameters params;
    private ColorBlobDetector mOpenCvCameraView;
    
    // UI elements
	Button findClientsButton;
	Button lockExposureButton;
    
    // Flags and notifiers
	private boolean checkIpFinished = true;
    private boolean networkError = false;
    private boolean gotFrame = false;
    private boolean doProcessing = true;
    private boolean configuredExposure = false;
    private int processedFrames = 15;
    
    // Constants
    private Scalar redColour = new Scalar(255, 0, 0, 255);
    private Scalar greenColour = new Scalar(0, 255, 0, 255);
    private Scalar whiteColour = new Scalar(255, 255, 255);
    private Point emptyPoint = new Point(0, 0);
    
    // Locations for text drawing
    private Point topTextPoint = new Point(10, 30);
    private Point bottomTextPoint = new Point(10, 60);
    private Point errorTextPoint = new Point(10, 120);
    
    // Contour variables
    List<MatOfPoint> redcontours;
	List<MatOfPoint> greencontours;
	
	// WiFi connection information
	private String arduinoIP = "192.168.43.70"; // 19 Rifdhan, 70 Steve
	private int arduinoIPEnding = 70;
	private String hostIP = "192.168.43.1";
	private String IpBase = "192.168.43.";
    
    // Arduino WiFi variables
	Socket socket = null;
	DataOutputStream dataOutputStream = null;
	DataInputStream dataInputStream = null;
	static public int result = -1;
	
	// Client WiFi variables
	private int j;
	List<Socket> clientSockets = null;
	List<DataOutputStream> clientOutputStreams = null;
	List<DataInputStream> clientInputStreams = null;
	Socket tempSocket = null;
	DataOutputStream tempDataOutputStream = null;
	DataInputStream tempDataInputStream = null;
	
	// Scaling and offset variables
	double a = 1, b = 0;
    
	// Initialization
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i("information", "OpenCV loaded successfully");
                    
                    if(modeOfOperation == 0 || modeOfOperation == 2) {
                    	mOpenCvCameraView.enableView();
                        mOpenCvCameraView.setOnTouchListener(ColorBlobDetectionActivity.this);
                    }
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    // Class initialization
    public ColorBlobDetectionActivity() {
        Log.i("information", "Instantiated new " + this.getClass());
    }
	
    // When the activity is started, open the camera and socket, and setup the view
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.color_blob_detection_surface_view);
        
        // Get buttons
        findClientsButton = (Button) findViewById(R.id.find_clients);
        lockExposureButton = (Button) findViewById(R.id.lock_auto_exposure_button);
        
        // Open network socket
        //new Thread(new OpenSocket()).start();
        
        // Get mode and scaling/offset
		modeOfOperation = MainMenuActivity.modeOfOperation;
		a = MainMenuActivity.a;
		b = MainMenuActivity.b;
        
		// Open connection to Arduino if applicable
		arduinoIP = ConnectToWiFiActivity.arduinoIp;
        if(modeOfOperation == 0 || modeOfOperation == 1) {
        	Thread openSocketThread = new Thread(new Runnable() {
            	@Override
            	public void run() {
            		Log.d("information", "Opening socket for Arduino communication");
            		
            		try {
            			if(modeOfOperation == 0 || modeOfOperation == 1) {
            				socket = new Socket(arduinoIP, 7);
            			} else if(modeOfOperation == 2) {
            				socket = new Socket(hostIP, 7);
            			}
            			
            			dataOutputStream = new DataOutputStream(socket.getOutputStream());
            			dataInputStream = new DataInputStream(socket.getInputStream());
            		} catch (UnknownHostException e) {
            			Log.d("error", "Error opening socket: unknown host exception");
            			e.printStackTrace();
            		} catch (IOException e) {
            			Log.d("error", "Error opening socket: input/output exception");
            			e.printStackTrace();
            		}
            	};
            });
            
            openSocketThread.start();
        }
        
        // Hide button if applicable
        if(modeOfOperation == 1) {
        	lockExposureButton.setVisibility(View.INVISIBLE);
        }
        
        // Hide button and begin processing images if applicable
        if(modeOfOperation == 0 || modeOfOperation == 2) {
        	findClientsButton.setVisibility(View.INVISIBLE);
        	processingThread.start();
    		
            mOpenCvCameraView = (ColorBlobDetector) findViewById(R.id.color_blob_detection_activity_surface_view);
            mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
            mOpenCvCameraView.setCvCameraViewListener(this);
        }
        
        // Display help pop-up
        helpPopup(null);
    }

    // When activity is paused
    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null) {
            mOpenCvCameraView.disableView();
        }
    }

    // When activity resumes from pause
    @Override
    public void onResume()
    {  
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
    }

    // When activity ends
    public void onDestroy() {
        super.onDestroy();
        
        // Close network socket
    	try {
			socket.close();
			dataOutputStream.close();
			dataInputStream.close();
		} catch (IOException e) {
			Log.d("error", "Error: I/O exception while closing socket");
			e.printStackTrace();
		}
    	
    	// Close camera object
        if (mOpenCvCameraView != null) {
            mOpenCvCameraView.disableView();
        }
    }
    
    // For the demo host mode, connect to available clients
    public void findClients(View v) {
        // For the host mode, wait for connections
        if(modeOfOperation == 1) {
        	for(j = 238; j == 238; j ++) {
        		if(j == arduinoIPEnding) {
            		Log.d("information", "Skipping Arduino's IP: " + j);
        			continue;
        		}
        		
        		Log.d("information", "Checking IP: " + j);
        		
        		tempSocket = null;
        		tempDataOutputStream = null;
        		tempDataInputStream = null;
        		
        		Thread clientSocketThread = new Thread(new Runnable() {
                	@Override
                	public void run() {
                		
                		try {
                			Log.d("information", "Entered check for IP: " + j);
                			tempSocket = new Socket(IpBase + j, 7);
                			
                			tempDataOutputStream = new DataOutputStream(tempSocket.getOutputStream());
                			tempDataInputStream = new DataInputStream(tempSocket.getInputStream());
                		} catch (Exception e) {
                			tempSocket = null;
                			Log.d("error", "Error opening connection");
                			e.printStackTrace();
                		}
                		
                		finally {
                			checkIpFinished = true;
                		}
                	};
                });
        		
        		checkIpFinished = false;
        		clientSocketThread.start();
        		
        		//while(!checkIpFinished);
        		
        		if(tempSocket != null) {
        			clientSockets.add(tempSocket);
        			clientOutputStreams.add(tempDataOutputStream);
        			clientInputStreams.add(tempDataInputStream);
        		}
        	}
        	
        	if(clientSockets != null) {
        		Log.d("information", "Total devices connected: " + clientSockets.size());
        	}
        }
    }

    // When camera begins, turn on auto-exposure to configure exposure
    public void onCameraViewStarted(int width, int height) {
    	// Initialize image variables
        newFrame = new Mat(height, width, CvType.CV_8UC4);
        lastProcessedFrame = new Mat(height, width, CvType.CV_8UC4);
        lastProcessedFrame.setTo(new Scalar(0, 0, 0, 0));
        currentFrame = new Mat(height, width, CvType.CV_8UC4);

        mOpenCvCameraView.configureCamera("lock camera"); // Disable some automatic camera features
        mOpenCvCameraView.configureCamera("unlock auto-exposure"); // Enable auto-exposure
        Toast.makeText(this, "Exposure-setting mode", Toast.LENGTH_SHORT).show();
    }

    // When camera stops, release images
    public void onCameraViewStopped() {
        newFrame.release();
        lastProcessedFrame.release();
    }

    // When screen is touched do nothing
    public boolean onTouch(View v, MotionEvent event) {
        return true;
    }
    
    // When a new frame comes from the camera, process it if possible
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		// Get new frame
		newFrame = inputFrame.rgba();
		
		// Set flag
		gotFrame = true;
		
		// Increment counter
		processedFrames++;
		
		// Return image to be drawn to the screen
		return lastProcessedFrame;
    }
    
    // Process camera frames as fast as possible
    Thread processingThread = new Thread(new Runnable() {
    	@Override
    	public void run() {
    		while(true) {
    			// Wait for first frame
    			while(!gotFrame);
    			
        		// Process the frame if applicable (grab a few frames before processing one)
        		if (doProcessing && processedFrames > 5) {
            		// Save current frame for processing
        			newFrame.copyTo(currentFrame);
        			
        			// Process the frame
        			mOpenCvCameraView.process(currentFrame);
        			
        			if(configuredExposure == true) {
        				doProcessing = false;
        				processedFrames = 0;
        			
        				Thread networkCommunicationThread = new Thread(new Runnable() {
        					@Override
        					public void run() {
        						try {
        							// Check if network connection is working
        							if(socket == null || dataInputStream == null || dataInputStream == null) {
        								Log.d("error", "Error: sockets and/or I/O streams are null - check Arduino's IP");
        								networkError = true;
        								return;
        							}
        							
        							// Get values to write to Arduino
        							int[] values = new int[6];
        							values = mOpenCvCameraView.getIndices(a, b);
        							
        							// Write the values to socket
        							for(int i = 0; i < 6; i ++) {
        								dataOutputStream.writeByte(values[i] + 48);
        							}

        							// Read byte from socket
        							result = dataInputStream.readByte();
    								
        							// Signal that program is ready for next frame
        							doProcessing = true;
        			        		processedFrames = 0;
        						} catch (UnknownHostException e) {
        							Log.d("error", "Error: unknown host exception during Arduino I/O");
        							e.printStackTrace();
        						} catch (IOException e) {
        							Log.d("error", "Error: inout/output exception during Arduino I/O");
        							e.printStackTrace();
        						}
        					};
        				});
        				
        				networkCommunicationThread.start();
        			}

        			// Get the contour information
        			redcontours = mOpenCvCameraView.getRedContours();
        			greencontours = mOpenCvCameraView.getGreenContours();

        			// Draw filed contours on top of the original camera image
        			Imgproc.drawContours(currentFrame, redcontours, -1, redColour, -1, 8, new Mat(), 500, emptyPoint);
        			Imgproc.drawContours(currentFrame, greencontours, -1, greenColour, -1, 8, new Mat(), 500, emptyPoint);
        			
        			// Get LED counts
        			int[] ledCounts;
        			ledCounts = mOpenCvCameraView.getLedCounts();

        			// Draw the LED counts as text on the image
        			Core.putText(currentFrame, "Red: " + ledCounts[1], topTextPoint, Core.FONT_HERSHEY_SIMPLEX, 1, whiteColour);
        			Core.putText(currentFrame, "Green: " + ledCounts[0], bottomTextPoint, Core.FONT_HERSHEY_SIMPLEX, 1, whiteColour);

        			// Check for network error and display it
        			if(networkError) {
        				Core.putText(currentFrame, "Network communication error", errorTextPoint, Core.FONT_HERSHEY_SIMPLEX, 1, whiteColour);
        			}
        			
        			// Save this as the latest frame
        			currentFrame.copyTo(lastProcessedFrame);
        			
        			// Do garbage collection to free up memory
        			System.gc();
        		}
    		}
    	};
    });
    
    // Locks auto-exposure to the current value
    public void lockAutoExposure(View v) {
    	Button lockButton = (Button) findViewById(R.id.lock_auto_exposure_button);
    	Button helpButton = (Button) findViewById(R.id.help_button);
    	
    	mOpenCvCameraView.configureCamera("lock auto-exposure");
        configuredExposure = true;
        Toast.makeText(this, "Locked auto-exposure", Toast.LENGTH_SHORT).show();
        
        //getLocalIpAddress();
        lockButton.setVisibility(View.INVISIBLE);
        helpButton.setVisibility(View.INVISIBLE);
    }
    
    // Help information
    public void helpPopup(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Instructions for setting up bugbroom:\n"
        		         + "1. Start WiFi hotspot on phone (done in previous screen). "
        		         + "If you are experiencing issues with this, set it up manually from phone settings.\n"
        		         + "2. Start or reset Arduino and wait for red and green test lights to turn on.\n"
        		         + "3. Put camera against an LED to lower exposure (dark as possible without LEDs flickering)\n"
        		         + "4. Set auto-exposure to locked (using the button in the bottom-right)\n"
        		         + "5. Start using the app to detect veillance")
               .setPositiveButton("OK", null);
        
        builder.create().show();
    }
    
    // Gets the IPs of all devices on the AP
    public String getLocalIpAddress() {
    	Log.d("info", "Printing IPs");
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        Log.d("IPs", "IPs: " + inetAddress.getHostAddress() );
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("error", "Error while getting IPs on the AP");
            ex.printStackTrace();
        }
        return null;
    }
} 
