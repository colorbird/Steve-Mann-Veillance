package org.opencv.samples.colorblobdetect;

import org.abaq.wirelessveillometer.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainMenuActivity extends Activity {
	
	// Operational information
	public static int modeOfOperation = 0;
	
	// Settings information
	public static double a = 1, b = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
	}
	
	// Launch single user mode activity
	public void goToSingleUser(View v) {
		modeOfOperation = 0;
		Intent switchActivity = new Intent(this, ConnectToWiFiActivity.class);
		startActivity(switchActivity);
	}
	
	// Launch multi-user host mode activity
	public void goToMultiUserHost(View v) {
		modeOfOperation = 1;
		Intent switchActivity = new Intent(this, ConnectToWiFiActivity.class);
		startActivity(switchActivity);
	}
	
	// Launch multi-user client mode activity
	public void goToMultiUserClient(View v) {
		modeOfOperation = 2;
		Intent switchActivity = new Intent(this, ConnectToWiFiActivity.class);
		startActivity(switchActivity);
	}
	
	// Open settings activity
	public void goToSettings(View v) {
		Intent switchActivity = new Intent(this, SettingsActivity.class);
		startActivity(switchActivity);
	}
	
	// Help information popup
	public void openHelp(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("This screen lets you select a mode to run this app in. Currently, the client-server demo mode is under development and not fully functional. "
        		         + "Please select the single-user mode to run the demo from this device.\n\n"
        		
        		         + "If you require help setting up the Arduino, see the \"Help with Arduino\" section in the main menu.\n\n"
        		         
        		         + "If you require help setting up the network connection, see the \"Help\" section in the \"Open A Network Connection\" screen.")
               .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       // On button click do nothing
                   }
               });
        
        builder.create().show();
	}
	
	// Arduino help information popup
	public void openArduinoHelp(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("The Arduino has five indicator lights to display its progress during initialization.\n\n"

        		         + "There are five stages during initialization:\n"
        		         + "1. First light turns on right at the start to ensure the LED strip is functioning.\n"
        				 + "2. Second light turns on when the WiFi shield is initialized.\n"
        				 + "3. Third light turns on when attempting to connect to WiFi network.\n"
        				 + "4. Fourth light turns on when attempting to get DHCP information.\n"
        				 + "5. Fifth light turns on when starting the network server.\n\n"
        		
        				 + "For each light:\n"
				 		 + "A blue LED indicates the stage is in progress.\n"
				 		 + "A red LED indicates an error occurred at the present stage.\n"
				 		 + "A green LED indicates the stage completed without issue.\n\n"
				 		 
        				 + "If you are still unsure what the issue is, connect the Arduino to a computer using a USB cable "
        				 + "and view the logs using the Serial Monitor utility (included in the Arduino IDE).")
               .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       // On button click do nothing
                   }
               });
        
        builder.create().show();
	}
}
